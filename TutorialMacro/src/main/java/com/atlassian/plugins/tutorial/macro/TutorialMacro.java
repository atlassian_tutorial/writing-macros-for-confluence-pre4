package com.atlassian.plugins.tutorial.macro;

import java.util.Map;
import java.util.List;
import java.util.Random;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.user.User;

/**
 * A macro showing the basics of macro writing in Confluence.
 */
public class TutorialMacro extends BaseMacro
{
    private static final String MACRO_BODY_TEMPLATE = "templates/tutorial-macro.vm";

    private final SpaceManager spaceManager;

    /**
     * Constructor. When the plugin containing the macro is activated, Confluence
     * will instantiate this class and automatically inject an implementation
     * of {@code SpaceManager}.
     * @param spaceManager the {@code SpaceManager} implementation to use
     */
    public TutorialMacro(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public boolean isInline()
    {
        // This macro is meant to render outside of a block element, so return
        // false.
        return false;
    }

    public boolean hasBody()
    {
        // This macro isn't meant to have any body text, so return false.
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        // This macro uses Velocity to render finished HTML, so no further
        // renderer massaging should be performed on the final result.
        return RenderMode.NO_RENDER;
    }

    public String execute(Map params, String body, RenderContext renderContext)
            throws MacroException
    {
        // create a Velocity context object as a model between the controller
        // (this macro) and the view (the Velocity template)
        Map<String, Object> context = MacroUtils.defaultVelocityContext();

        // check if the user supplied a "greeting" parameter
        if (params.containsKey("greeting"))
        {
            context.put("greeting", params.get("greeting"));
        }
        else
        {
            // we'll construct one. get the currently logged in user and display
            // their name
            User user = AuthenticatedUserThreadLocal.getUser();
            if (user != null)
            {
                context.put("greeting", "Hello " + user.getFullName());
            }
        }

        // get all spaces in this installation
        @SuppressWarnings("unchecked")
            List<Space> spaces = spaceManager.getAllSpaces();
        context.put("totalSpaces", spaces.size());

        if (!spaces.isEmpty())
        {
            // pick a space at random and find its home page
            Random random = new Random();
            int randomSpaceIndex = random.nextInt(spaces.size());
            Space randomSpace = spaces.get(randomSpaceIndex);
            context.put("spaceName", randomSpace.getName());

            Page homePage = randomSpace.getHomePage();
            context.put("homePageTitle", homePage.getTitle());
            context.put("homePageCreator", homePage.getCreatorName());
        }

        // render the Velocity template with the assembled context
        return VelocityUtils.getRenderedTemplate(MACRO_BODY_TEMPLATE, context);

    }

}